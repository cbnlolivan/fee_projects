// Chờ tài liệu HTML được tải hoàn toàn trước khi thực thi script
document.addEventListener('DOMContentLoaded', function () {
    var currentAccount = sessionStorage.getItem("currentAccount");
    if (currentAccount == null) {
        currentAccount = "";
    }
    $("#email").val(currentAccount);

    // Lấy tham chiếu đến biểu mẫu và div kết quả
    const form = document.getElementById('edit-form');
    const resultDiv = document.getElementById('result');

    // Thêm một trình nghe sự kiện cho sự kiện nộp biểu mẫu
    form.addEventListener('submit', function (event) {
        // Ngăn chặn hành vi mặc định của sự kiện nộp biểu mẫu
        event.preventDefault();

        // Lấy giá trị
        const firstname = document.getElementById('first-name-input').value;
        const lastname = document.getElementById('last-name-input').value;
        const phone = document.getElementById('phone-input').value;
        const description = document.getElementById('desciption-input').value;

        // 
        // Check name format using regular expression
        const nameRegex = /^[a-zA-Z ]{3,30}$/;
        if (!nameRegex.test(firstname)) {
            // If firstname doesn't match the regex, display a red error message
            hienThiKetQua('Wrong format of first name!', 'red');
            return;
        }
        if (!nameRegex.test(lastname)) {
            // If lastname doesn't match the regex, display a red error message
            hienThiKetQua('Wrong format of last name!', 'red');
            return;
        }

        // Check phone format using regular expression
        const phoneRegex = /^0\d{9}$/;
        if (!phoneRegex.test(phone)) {
            // If email doesn't match the regex, display a red error message
            hienThiKetQua('Wrong format of phone number!', 'red');
            return;
        }

        // Check Description is under 200 characters using regular expression
        const desRegex = /^.{0,200}$/;
        if (!desRegex.test(description)) {
            // If password doesn't match the regex, display a red error message
            hienThiKetQua('Description too long!', 'red');
            return;
        }

        var currentAccount = $("#email").val();
        var accounts = getCookie('accounts');
        if (!accounts) {
            accounts = [];
        } else {
            accounts = JSON.parse(accounts);
        }

        for (let index = 0; index < accounts.length; index++) {
            if (accounts[index]["account"] == currentAccount) {
                accounts[index]["username"] = firstname + " " + lastname;
                accounts[index]["phone"] = phone;
                accounts[index]["description"] = description;
                debugger;
                accounts.splice(index, 1, accounts[index]);
                console.log(accounts);
                setCookie("accounts", JSON.stringify(accounts), 30);
            }

        }

        // Hiển thị thông báo thành công
        hienThiKetQua('Succesfully update information!', 'green');
        setInterval(function () {
            location.reload();
        }, 4000);
    });

    // Set value to cookie
    function setCookie(name, value, days) {
        var expires = "";

        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    }

    // Hàm để lấy giá trị cookie theo tên
    function getCookie(name) {
        const cookies = document.cookie.split(';');
        for (let cookie of cookies) {
            const [cookieName, cookieValue] = cookie.split('=');
            if (cookieName.trim() === name) {
                return cookieValue;
            }
        }
        return null;
    }

    // Hàm để hiển thị thông báo kết quả
    function hienThiKetQua(message, color) {
        resultDiv.style.color = color;
        resultDiv.innerHTML = message;
        resultDiv.style.display = 'block';
    }
});
