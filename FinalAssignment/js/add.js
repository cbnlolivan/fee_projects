// Chờ tài liệu HTML được tải hoàn toàn trước khi thực thi script
document.addEventListener('DOMContentLoaded', function () {
    // Lấy tham chiếu đến biểu mẫu và div kết quả
    const form = document.getElementById('add-form');
    const resultDiv = document.getElementById('result');

    // Thêm một trình nghe sự kiện cho sự kiện nộp biểu mẫu
    form.addEventListener('submit', function (event) {
        // Ngăn chặn hành vi mặc định của sự kiện nộp biểu mẫu
        event.preventDefault();

        // Lấy giá trị
        const title = document.getElementById('title-input').value;
        const brief = document.getElementById('brief-input').value;
        const content = document.getElementById('content-input').value;

        // Check username format using regular expression
        const titleRegex = /^.{10,200}$/;
        if (!titleRegex.test(title)) {
            // If username doesn't match the regex, display a red error message
            hienThiKetQua('Title must be between 10 and 200 characters!', 'red');
            return;
        }

        // Check email format using regular expression
        const briefRegex = /^.{30,150}$/;
        if (!briefRegex.test(brief)) {
            // If email doesn't match the regex, display a red error message
            hienThiKetQua('Brief must be between 30 and 150 characters!', 'red');
            return;
        }

        // Check password format using regular expression
        const contentRegex = /^.{50,1000}$/;
        if (!contentRegex.test(content)) {
            // If password doesn't match the regex, display a red error message
            hienThiKetQua('Content must be between 50 and 1000 characters!', 'red');
            return;
        }

        var newcontent = [{ title: title, brief: brief, description: content, createdDate: new Date().toLocaleString() }];
        var listcontents = getCookie('contents');
        if (!listcontents) {
            listcontents = [];
        } else {
            listcontents = JSON.parse(listcontents);
        }
        listcontents = listcontents.concat(newcontent);
        setCookie("contents", JSON.stringify(listcontents), 30);


        $.ajax({
            url: "viewcontent.html",
            type: "get", //send it through get method
            data: {
            },
            success: function () {
                // location.href = "viewcontent.html"
            },
            error: function (xhr) {
                //Do Something to handle error
            }
        });

        // Hiển thị thông báo thành công
        hienThiKetQua(`Succesfully add a content!`, 'green');
    });


    // Hàm để lấy giá trị cookie theo tên
    function getCookie(name) {
        const cookies = document.cookie.split(';');
        for (let cookie of cookies) {
            const [cookieName, cookieValue] = cookie.split('=');
            if (cookieName.trim() === name) {
                return cookieValue;
            }
        }
        return null;
    }
    // Set value to cookie
    function setCookie(name, value, days) {
        var expires = "";

        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    }

    // Hàm để hiển thị thông báo kết quả
    function hienThiKetQua(message, color) {
        resultDiv.style.color = color;
        resultDiv.innerHTML = message;
        resultDiv.style.display = 'block';
    }
});

