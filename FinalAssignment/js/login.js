// Chờ tài liệu HTML được tải hoàn toàn trước khi thực thi script
document.addEventListener('DOMContentLoaded', function () {
    // Autofill password (if it had been saved in the cookies)
    document.getElementById("email").addEventListener('change', function autoFillPassword() {
        var email = document.getElementById('email').value;
        var sessionAccount = sessionStorage.getItem(email);
        if (sessionAccount == null) {
            sessionAccount = "";
        }
        if (sessionAccount != null && sessionAccount != "") {
            $('#password').val(sessionAccount);
            $("#savepass").prop('checked', true);
        }
    });
    // Lấy tham chiếu đến biểu mẫu và div kết quả
    const form = document.getElementById('login-form');
    const resultDiv = document.getElementById('result');
    // Thêm một trình nghe sự kiện cho sự kiện nộp biểu mẫu
    form.addEventListener('submit', function (event) {

        // Ngăn chặn hành vi mặc định của sự kiện nộp biểu mẫu
        event.preventDefault();

        // Lấy giá trị của ô nhập email
        const email = document.getElementById('email').value;
        var pass = document.getElementById('password').value;
        const savepass = document.getElementById('savepass');

        // List of accounts (email and pass) which are saved in the cookies "accounts"
        var accounts = getCookie('accounts');
        if (!accounts) {
            accounts = [];
        } else {
            accounts = JSON.parse(accounts);
        }

        var accountSession = sessionStorage.getItem(email);
        for (var item of accounts) {
            var emailCookie = item["account"];
            if (email == emailCookie) {
                if (accountSession == null) {
                    accountSession = "";
                }
                if (!savepass.checked) {
                    accountSession = "";
                } else {
                    accountSession = pass;
                }
                
                if (pass != item["pass"]) {
                    hienThiKetQua('Wrong password!', 'red');
                    return;
                }

                sessionStorage.setItem(email, accountSession);
                sessionStorage.setItem("currentAccount", email);

                location.href = '/FinalAssignment/viewcontent.html';
                return;
            }
        }
        hienThiKetQua('This email is not registed!', 'red');
        
        return;
    });


    // Hàm để lấy giá trị cookie theo tên
    function getCookie(name) {
        const cookies = document.cookie.split(';');
        for (let cookie of cookies) {
            const [cookieName, cookieValue] = cookie.split('=');
            if (cookieName.trim() === name) {
                return cookieValue;
            }
        }
        return null;
    }

    // Hàm để hiển thị thông báo kết quả
    function hienThiKetQua(message, color) {
        resultDiv.style.color = color;
        resultDiv.innerHTML = message;
        resultDiv.style.display = 'block';
    }
});
