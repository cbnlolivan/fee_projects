// Chờ tài liệu HTML được tải hoàn toàn trước khi thực thi script
document.addEventListener('DOMContentLoaded', function () {
    // Lấy tham chiếu đến biểu mẫu và div kết quả
    const form = document.getElementById('register-form');
    const resultDiv = document.getElementById('result');

    // Thêm một trình nghe sự kiện cho sự kiện nộp biểu mẫu
    form.addEventListener('submit', function (event) {
        // Ngăn chặn hành vi mặc định của sự kiện nộp biểu mẫu
        event.preventDefault();

        // Lấy giá trị
        const username = document.getElementById('username').value;
        const email = document.getElementById('email').value;
        const password = document.getElementById('password').value;
        const repass = document.getElementById('re-password').value;
        var accounts = getCookie('accounts');
        if (!accounts) {
            accounts = [];
        } else {
            accounts = JSON.parse(accounts);
        }

        for (var item in accounts) {
            if (accounts[item]["account"] === email) {
                hienThiKetQua('This email had been registed!', 'red');
                return;
            }
        }
        // var emailCookie = getCookie(email);
        // if (emailCookie != null) {
        //     hienThiKetQua('This email had been registed!', 'red');
        //     return;
        // }

        // Check username format using regular expression
        const nameRegex = /^[a-zA-Z ]{3,30}$/;
        if (!nameRegex.test(username)) {
            // If username doesn't match the regex, display a red error message
            hienThiKetQua('Wrong format of username!', 'red');
            return;
        }

        // Check email format using regular expression
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        if (!emailRegex.test(email)) {
            // If email doesn't match the regex, display a red error message
            hienThiKetQua('Địa chỉ email không hợp lệ', 'red');
            return;
        }

        // Check password format using regular expression
        const passRegex = /^.{8,30}$/;
        if (!passRegex.test(password)) {
            // If password doesn't match the regex, display a red error message
            hienThiKetQua('Wrong format password', 'red');
            return;
        }

        //Check whether the re-password match or not
        if (repass != password) {
            // If re-password doesn't match the password, display a red error message
            hienThiKetQua('Re-password not match!', 'red');
            return;
        }
        var newaccount = [{ username: username, account: email, pass: password, phone: "", description: ""}];
        var accounts = getCookie('accounts');
        if (!accounts) {
            accounts = [];
        } else {
            accounts = JSON.parse(accounts);
        }
        accounts = accounts.concat(newaccount);
        setCookie("accounts", JSON.stringify(accounts), 30);

        // Hiển thị thông báo thành công
        $("#username").val("");
        $("#password").val("");
        $("#re-password").val("");
        $("#email").val("");
        hienThiKetQua(`Succesfully register an account with email: ${email}`, 'green');

    });

    // Hàm để hiển thị thông báo kết quả
    function hienThiKetQua(message, color) {
        resultDiv.style.color = color;
        resultDiv.innerHTML = message;
        resultDiv.style.display = 'block';
    }
});
// Hàm để lấy giá trị cookie theo tên
function getCookie(name) {
    const cookies = document.cookie.split(';');
    for (let cookie of cookies) {
        const [cookieName, cookieValue] = cookie.split('=');
        if (cookieName.trim() === name) {
            return cookieValue;
        }
    }
    return null;
}
// Set value to cookie
function setCookie(name, value, days) {
    var expires = "";

    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}



