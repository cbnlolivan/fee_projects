document.addEventListener('DOMContentLoaded', function () {
    var x = document.getElementsByClassName("content-right")[0];
    var y = document.getElementsByClassName("content-right")[1];
    x.style.display = "none";
    y.style.display = "block";

    setInterval(function () {
        x.style.display = "block";
        y.style.display = "none";
    }, 1000);
    showContent();
})

function showContent() {
    var data = getCookie("contents");
    if (!data) {
        return;
    }
    data = JSON.parse(data);
    var tbody = $("#table_content tbody");
    data.forEach((element, index) => {
        var row = "<tr><td>" + (index + 1) + "</td><td>" + element.title + "</td><td>" + element.brief + "</td><td>" + element.createdDate + "</td></tr>";
        tbody.append(row);
    });
}

// Hàm để lấy giá trị cookie theo tên
function getCookie(name) {
    const cookies = document.cookie.split(';');
    for (let cookie of cookies) {
        const [cookieName, cookieValue] = cookie.split('=');
        if (cookieName.trim() === name) {
            return cookieValue;
        }
    }
    return null;
}